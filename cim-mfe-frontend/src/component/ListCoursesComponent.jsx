import React, {Component} from 'react'
import CourseDataService from '../service/CourseDataService';

class ListCoursesComponent extends Component{
  

    constructor(props){
        super(props)
        this.state = {
            courses: [],
            message: null,
            addNotAllowed: false
        }
        this.refreshCourses = this.refreshCourses.bind(this)
        this.deleteClickedCourse = this.deleteClickedCourse.bind(this)
        this.updateClickedCourse = this.updateClickedCourse.bind(this)
        this.addCourseClicked = this.addCourseClicked.bind(this)
    }

    componentDidMount(){
        console.log("Mounting List Courses...")
        this.setState({addNotAllowed: true})
        this.refreshCourses();
    }

    refreshCourses(){
        CourseDataService.retrieveAllCourses("ramyhassan").then(
            response => {
                console.log(response);
                this.setState({courses: response.data})
            }
        )
    }

    deleteClickedCourse(id){
        CourseDataService.deleteCourse("ramyhassan", id).then(
            response => {
                this.setState({message: `Delete Course ${id} done successfully`})
                this.refreshCourses()
            }
        )
    }

    updateClickedCourse(id){
        console.log('updating  '+ id)
        this.props.history.push(`/courses/${id}`)
    }

    addCourseClicked(){
        this.props.history.push(`/courses/-1`)
    }

    render(){
        return(

            <div className="container">
                <h3>All Cources</h3>
        {this.state.message && <div class ="alert alert-success">{this.state.message}</div>}
                <table className="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.courses.map(
                                course =>
                                 <tr key={course.id}>
                                     <td>{course.id}</td>
                            <td>{course.description}</td>
                            <td><button className="btn btn-warning" onClick={() => this.deleteClickedCourse(course.id)}>Delete</button></td>
                            <td><button className="btn btn-success" onClick={() => this.updateClickedCourse(course.id) }>Update</button></td>

                                </tr>
                            )
                        }
                    </tbody>
                </table>
                <div className="row" hidden={this.state.addNotAllowed}>
                    <button className="btn btn-success" onClick={this.addCourseClicked} >Add</button>
                </div>
            </div>
        );
    }
}

export default ListCoursesComponent