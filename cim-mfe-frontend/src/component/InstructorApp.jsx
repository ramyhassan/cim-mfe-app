import React, {Component} from 'react';
import ListCoursesComponent from './ListCoursesComponent';
import CourseComponent from './CourseComponent'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LoginComponent from './LoginComponent'
import LogoutComponent from './LogoutComponent'
import MenuComponent from './MenuComponent'
import AuthenticatedRoute from './AuthenticatedRoute';

class InstructorApp extends Component{

    componentDidMount(){
      console.log("Mounting Main App ...");
    }


    render(){
        return (
        <Router>
            <>
            <h1> Instructor Application</h1>
            <MenuComponent />
            <Switch>
                <Route path="/" exact component={LoginComponent}></Route>
                <Route path="/login" exact component={LoginComponent}></Route>
                <Route path="/logout" exact component={LogoutComponent}></Route>
                <AuthenticatedRoute path="/courses" exact component={ListCoursesComponent} />
                <AuthenticatedRoute path="/courses/:id" exact component={CourseComponent}/>
            </Switch>

            </>
        </Router>
            )
    }
}

export default InstructorApp

