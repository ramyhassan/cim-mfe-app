import React, { Component } from 'react'
import { Route, Redirect } from 'react-router-dom'
import AuthenticationService from '../service/AuthenticationService';

class AuthenticatedRoute extends Component {
    constructor(){
        super()
        console.log('Construct Authenticated Router')
        AuthenticationService.setupAxiosInterceptors(sessionStorage.getItem('Token'));
        
    }



    render() {
        if (AuthenticationService.isUserLoggedIn()) {
            console.log('user is authenticated !!');
            return <Route {...this.props} />
        } else {
            return <Redirect to="/login" />
        }

    }
}

export default AuthenticatedRoute