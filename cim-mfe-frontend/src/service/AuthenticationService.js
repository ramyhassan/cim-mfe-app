import Axios from "axios";
const COURSES_API_URL ='http://localhost:8080'
const USER_NAME_SESSION_ATTRIBUTE_NAME = 'USER_NAME'
const TOKEN_KEY = 'Token'

class AuthenticationService{

    executeBasicAuthenticationService(username, password){
        return Axios.get(`${COURSES_API_URL}/basicauth`, { headers: { authorization: this.createBasicAuthToken(username, password) } })
    }

    executeJwtAuthenticationService(username, password) {
        console.log('will authenticate' + username);
        return Axios.post(`${COURSES_API_URL}/authenticate`, {
            username,
            password
        })
    }

    createBasicAuthToken(username, password){
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    registerSuccessfulLogin(username, password){
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createBasicAuthToken(username, password))
    }

    registerSuccessfulLoginForJwt(username, token) {
        console.log('Successful Login, register in session storage !!');
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username)
        this.setupAxiosInterceptors(this.createJWTToken(token))
    }

    createJWTToken(token) {
        sessionStorage.setItem(TOKEN_KEY,'Bearer ' + token)
        return 'Bearer ' + token
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME)
        if (user === null) return false
        return true
    }

    logout() {
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
    }

    setupAxiosInterceptors(token) {
        console.log('setup Axios interceptors:::' + token)
        Axios.interceptors.request.use(
            (config) => {
                if (this.isUserLoggedIn()) {
                    console.log('Yes User is logged in'+token)
                    config.headers.authorization = token
                }
                return config
            }
        )
    }
}

export default new AuthenticationService()