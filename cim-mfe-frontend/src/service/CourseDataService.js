import axios from 'axios'

const INSTRUCTOR = 'ramyhassan'
const COURSES_API_URL ='http://localhost:8085'
const INSTRUCTOR_API_URL = `${COURSES_API_URL}/instructor/${INSTRUCTOR}`

class CourseDataService{
    retrieveAllCourses(name){
        return axios.get(`${INSTRUCTOR_API_URL}/courses`)
    }


    createCourse(name, course){
        return axios.post(`${INSTRUCTOR_API_URL}/courses/`, course)
    }

    updateCourse(name, id, course){
        return axios.put(`${INSTRUCTOR_API_URL}/courses/${id}`, course)
    }

    deleteCourse(name, id){
        return axios.delete(`${INSTRUCTOR_API_URL}/courses/${id}`)
    }

    retrieveCourse(name, id){
        return axios.get(`${INSTRUCTOR_API_URL}/courses/${id}`)

    }
}

export default new CourseDataService()
