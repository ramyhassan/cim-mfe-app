package com.ramy.jwt;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@CrossOrigin(origins={ "http://localhost:3000", "http://localhost:4200" })
@RestController
public class JwtAuthenticationRestController {

    @Value("${jwt.http.request.header}")
    private String tokenHeader;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtInMemoryUserDetailsService jwtInMemoryUserDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping(path = "${jwt.get.token.uri}")
    public ResponseEntity<JwtTokenResponse> createAuthenticationToken(@RequestBody JwtTokenRequest authRequest) throws AuthenticationException
    {
        this.authenticate(authRequest.getUsername(), authRequest.getPassword());

        UserDetails userDetails = jwtInMemoryUserDetailsService.loadUserByUsername(authRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtTokenResponse(token));
    }

    @GetMapping(path = "${jwt.refresh.token.uri}")
    public ResponseEntity<JwtTokenResponse> refreshAndGetAuthenticationtoken(HttpServletRequest request){

        String token = getTokenFromHttpHeader(request);
        String username = jwtTokenUtil.getUsernameFromToken(token);

        JwtUserDetails user = (JwtUserDetails) jwtInMemoryUserDetailsService.loadUserByUsername(username);
        if (jwtTokenUtil.canTokenBeRefreshed(token)) {
            String refreshedToken = jwtTokenUtil.refreshToken(token);
            return ResponseEntity.ok(new JwtTokenResponse(refreshedToken));
        } else {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity<String> handleAuthenticationException(AuthenticationException authex){
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(authex.getMessage());
    }

    private void authenticate (String username, String password){
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        if(username.equals("ramyhassan") && password.equals("ay7aga123")){
            System.out.println("Tamam");
        }
        else{
            throw new AuthenticationException("INVALID_CREDENTIALS");
        }

    }

    private String getTokenFromHttpHeader(HttpServletRequest request){
        String authToken = request.getHeader(tokenHeader);
        return authToken.substring(7);
    }


}
