package com.ramy.course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
@RestController
public class CourseResource {

    @Autowired
    private CoursesHardcodedService courseManagmentService;

    @GetMapping("/instructor/{username}/courses/{courseId}")
    public Course getCourse(@PathVariable String username, @PathVariable long courseId){
        return courseManagmentService.findByID(courseId);
    }

    @GetMapping("/instructor/{username}/courses")
    public List<Course> getAllCourses(@PathVariable String username){
        return courseManagmentService.findAll();
    }

    @PutMapping("/instructor/{username}/courses/{courseId}")
    public ResponseEntity<Course> updateCourse(@PathVariable String username, @PathVariable long courseId, @RequestBody Course course){
        Course savedCourse = courseManagmentService.save(course);
        return new ResponseEntity<Course>(savedCourse, HttpStatus.OK);
    }

    @PostMapping("/instructor/{username}/courses")
    public ResponseEntity<Void> createCourse(@PathVariable String username, @RequestBody Course course){
        Course savedCourse = courseManagmentService.save(course);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/courseId").buildAndExpand(savedCourse.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/instructor/{username}/courses/{courseId}")
    public ResponseEntity<Void> deleteCourse(@PathVariable String username, @PathVariable long courseId){
       Course course=  courseManagmentService.deleteById(courseId);
       if(course !=null){
           return ResponseEntity.noContent().build();
       }

       return ResponseEntity.notFound().build();
    }
}
