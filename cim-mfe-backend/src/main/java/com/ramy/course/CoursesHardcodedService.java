package com.ramy.course;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CoursesHardcodedService {

    private static List<Course> courses = new ArrayList<>();
    private static long idCounter = 0;
    static{
        courses.add(new Course(++idCounter, "ramyhassan", "Learn Full stack with Spring Boot and Angular"));
        courses.add(new Course(++idCounter, "ramyhassan", "Learn Full stack with Spring Boot and React"));
        courses.add(new Course(++idCounter, "ramyhassan", "Master Microservices with Spring Boot and Spring Cloud"));
        courses.add(new Course(++idCounter, "ramyhassan",
                "Deploy Spring Boot Microservices to Cloud with Docker and Kubernetes"));
    }

    public List<Course> findAll() {
        return courses;
    }

    public Course findByID(long id){
      return courses.stream().filter(course -> course.getId() == id).findFirst().get();
    }

    public Course save(Course course) {
        if (course.getId() == -1 || course.getId() == 0) {
            course.setId(++idCounter);
            courses.add(course);
        } else {
            deleteById(course.getId());
            courses.add(course);
        }
        return course;
    }

    public Course deleteById(long id){
        Course course = findByID(id);
        if (course ==null){
            return null;
        }
        if(courses.remove(course)){
            return course;
        }
        return null;
    }
}
